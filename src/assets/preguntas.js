export const preguntas = [

    {
        "id": "UNO",
        "tipoPregunta": "select",
        "pregunta": "1. Al pensar en su experiencia más reciente con Lucasian Labs, ¿el nivel de calidad del servicio que recibió fue?",
        "options": [
            "Sobresaliente",
            "Aceptable",
            "Discreto",
            "Deficiente"
        ]
    },
    {
        "id": "DOS",
        "tipoPregunta": "select",
        "pregunta": "2. A nivel general, durante la ejecución del último servicio que le proporcionamos, usted diría que Lucasian Labs cumplió los compromisos: antes, conforme o después de lo acordado?",
        "options": [
            "Antes de lo acordado",
            "Conforme a lo acordado",
            "Después de lo acordado",
            "No cumplió"
        ]
    },
    {
        "id": "TRES",
        "tipoPregunta": "select",
        "pregunta": "3. ¿En qué porcentaje Lucasian Labs fue puntual en la asistencia a las citas acordadas?",
        "options": [
            "Mayor al 80%",
            "Entre 80 y 50%",
            "Menor al 50%"
        ]
    },
    {
        "id": "CUATRO",
        "tipoPregunta": "checkbox",
        "pregunta": "4. A lo largo de la prestación del servicio mas reciente, ¿cuál de la siguiente información recibió de manera oportuna en relación con dicho servicio?",
        "options": [
            "Avances",
            "Desempeño",
            "Dificultades",
            "Victorias conseguidas",
            "Resultados",
            "Otras",
            "Ninguna de las anteriores"
        ],
        respuestas: []
    },
    {
        "id": "CINCO",
        "tipoPregunta": "rating",
        "pregunta": "5. En una escala de 1 a 5, usted diría que el índice resultante de la relación costo-beneficio del último servicio prestado por Lucasian Labs, se ubica en:",
        "options": [
            "option1",
            "option2",
            "option3",
            "option4",
            "option5"
        ]
    },
    {
        "id": "SEIS",
        "tipoPregunta": "select",
        "pregunta": "6. ¿Siente que las opiniones, inquietudes y/o sugerencias que tuvo durante el curso del último servicio suministrado por Lucasian Labs fueron consideradas?",
        "options": [
            "Si",
            "No",
            "No tuve conocimiento del medio/canal para comunicar mis inquietudes y/o sugerencias"
        ]
    },
    {
        "id": "SIETE",
        "tipoPregunta": "select",
        "pregunta": "7. Usted considera que el grado de idoneidad del recurso humano que Lucasian Labs dispuso para el desarrollo del último servicio fue:",
        "options": [
            "Altamente Idóneo",
            "Idóneo",
            "Poco Idóneo",
            "No era Idóneo"
        ]
    },
    {
        "id": "OCHO",
        "tipoPregunta": "select",
        "pregunta": "8. Partiendo de los principios y valores universales, usted considera que la calidad humana que Lucasian Labs reflejó durante la prestación del último servicio fue: ",
        "options": [
            "Propia con los principios y valores universales",
            "Al margen con los principios y valores universales",
            "Impropia con los principios y valores universales"
        ]
    },
    {
        "id": "NUEVE",
        "tipoPregunta": "select",
        "pregunta": "9. En comparación con otras alternativas del mercado de la misma categoría del último servicio prestado, usted diría que nuestro servicios es: ",
        "options": [
            "Mucho mejor",
            "Algo mejor",
            "Mas o menos igual",
            "Algo peor",
            "Mucho peor",
            "No lo sé"
        ]
    },
    {
        "id": "DIEZ",
        "tipoPregunta": "textarea",
        "pregunta": "10. ¿Qué pudimos haber hecho para que su experiencia con nuestro servicio hubiera sido superior?",
    }

];